
Data = new Mongo.Collection("DataList");
VenueData = new Mongo.Collection("VenueDataList");




if (Meteor.isClient) {

  Template.body.helpers({
    Data: function(){
       return Data.find({});
    }
  });

  Template.body.helpers({
    VenueData: function(){
       return VenueData.find({});
    }
  });

  Template.body.events({
     'click #submitvenue': function () {
    VenueData.insert({venuename : $("#venuenameinput").val(),
                 address : $("#addresstextarear").val(),
                 state : $('select[name=state]').val(),
                 city : $('select[name=city]').val()
               });
  },

    'click .adduniv': function () {
    Data.insert({text : $("#adds").val(), createdAt:"University of New York"});
  },

  "click .delete": function () {
    
      VenueData.remove(this._id);
    },
    'click #popupBoxClose': function(){
            $('#popup_box').fadeOut("slow");
            $("#container").css({ // this is just for style        
                "opacity": "1"  
            }); 
            $('#overlay-back').css("display","none") 
    },
    'click #addvenue': function(){
       $('#popup_box').show(); 
       $('#overlay, #overlay-back').fadeIn(500); 
       $('##overlay-back').show();
         $('#popup_box').fadeIn("slow");
            $("#container").css({ // this is just for style
                "opacity": "0.3"  
            });     
    }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
